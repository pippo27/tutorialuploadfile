//
//  ViewController.swift
//  TutorialUploadFile
//
//  Created by Arthit Thongpan on 4/30/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    let serviceURL = NSURL(string: "http://localhost/workshop_uploadfile/script.php")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let filePath: NSURL? = NSBundle(forClass: self.dynamicType).URLForResource("Sample", withExtension: "m4v")
        uploadFile(filePath!)
    }
    
    func uploadFile(filePath: NSURL)
    {
        guard let fileData:NSData = NSFileManager.defaultManager().contentsAtPath(filePath.path!) else {
            let title = NSLocalizedString("Error", comment: "")
            let message = NSLocalizedString("File not found.", comment: "")
            let cancelButtonTitle = NSLocalizedString("OK", comment: "")
            UIAlertController.showAlertControllerOnHostController(self,
                                                                  title: title, message: message,
                                                                  buttonTitle: cancelButtonTitle)
            return
        }
        
        let mutableURLRequest = NSMutableURLRequest(URL: serviceURL!)
        mutableURLRequest.HTTPMethod = "POST"
        
        let fileName = "Sample.m4v"   //ต้องการให้ php save เป็นชื่อไฟล์อะไร
        let mimeType = "audio/mpeg"     // Content-Type
        let phpVariable = "uploadFile"  // ตัวแปรสำหรับให้ php อ้างอิงถึง content ข้างใน
        
        let boundary = generateBoundaryString()
        
        let contentType = "multipart/form-data; boundary=" + boundary
        let boundaryStart = "--\(boundary)\r\n"
        let boundaryEnd = "--\(boundary)--\r\n"
        let contentDispositionString = "Content-Disposition: form-data; name=\"\(phpVariable)\"; filename=\"\(fileName)\"\r\n"
        let contentTypeString = "Content-Type: \(mimeType)\r\n\r\n"
        
        // Prepare the HTTPBody for the request.
        let requestBodyData : NSMutableData = NSMutableData()
        requestBodyData.appendData(boundaryStart.dataUsingEncoding(NSUTF8StringEncoding)!)
        requestBodyData.appendData(contentDispositionString.dataUsingEncoding(NSUTF8StringEncoding)!)
        requestBodyData.appendData(contentTypeString.dataUsingEncoding(NSUTF8StringEncoding)!)
        requestBodyData.appendData(fileData)
        requestBodyData.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        requestBodyData.appendData(boundaryEnd.dataUsingEncoding(NSUTF8StringEncoding)!)
        
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        mutableURLRequest.HTTPBody = requestBodyData
        let session = NSURLSession.sharedSession()
        
        
        let task = session.dataTaskWithRequest(mutableURLRequest) {
            (
            let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("error")
                return
            }
            
            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            
            print(dataString!)
            
        }
        
        task.resume()
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().UUIDString)"
    }
    
}

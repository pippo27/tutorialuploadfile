//
//  UIAlertControllerExtension.swift
//  BasicUploadFile
//
//  Created by Arthit Thongpan on 4/30/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit

extension UIAlertController{
    class func showAlertControllerOnHostController(
        hostViewController: UIViewController,
        title: String,
        message: String,
        buttonTitle: String){
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        controller.addAction(UIAlertAction(title: buttonTitle, style: .Default, handler: nil))
        hostViewController.presentViewController(controller, animated: true, completion: nil)
    }
}